<?php

namespace server\config;

// $url = "tcp://0.0.0.0:8000"; //"udp://127.0.0.1:1113"; //"tcp://0.0.0.0:8000";

$httpHeaders =  "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\n\r\n";

$fields = [
    /*top horizontal line*/
    0 => 'x5',
    1 => 'x15',
    2 => 'x15',
    3 => 'x30',
    4 => 'x30',
    5 => 'x10',
    6 => 'handcuffs',
    /*right vertical line*/
    7 => 'back',
    8 => 'x50',
    9 => 'x50',
    /*bottom horizontal line*/
    10 => 'water', //handcuffs?
    11 => 'x20',
    12 => 'up',
    13 => 'x80',
    14 => 'x80',
    15 => 'x30',
    16 => 'police',
    /*left vertical line*/
    17 => 'x120',
    18 => 'x120',
    19 => 'x200',
];
