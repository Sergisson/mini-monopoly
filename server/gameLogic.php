<?php
/*
author: Ablouhov Sergy
email: abloyxob at gmail dot com
*/

/*************************** Initialization ***********************************/

namespace server\gameLogic;

require_once('config.php');

// клиент посылает серверу:
// numberOfMoves - количество сделанных ходов
// remainingNumberOfMoves - количество оставшихся ходов
// playerPosition - конечная позиция игрока
// bonusWin - очки за текущий ход
// totalSum - сумма очков

$numberOfMoves = 0;
$remainingNumberOfMoves = 7;
$playerPosition = 0;
$bonusWin = 0;
$totalSum = 0;

$arrNumberOfMoves = [];
$arrRemainingNumberOfMoves =[];
$arrPlayerPosition = [];
$arrBonusWin = [];
$arrTotalSum = [];

/*************************** Function *****************************************/

function dieRoll()
{
    return rand(1, 6);
}

function reductionNumberStrokes()
{
    global $remainingNumberOfMoves;
    $remainingNumberOfMoves--;
}

function increaseNumberOfMoves()
{
    global $numberOfMoves;
    $numberOfMoves++;
}

function changePlayerPositions()
{
    /*
    x => x15/x20 (and others) points
    h  => handcuffs
    w  => water
    u => up
    b => back
    p => police
    */

    global $playerPosition, $fields, $bonusWin, $totalSum;

    $playerPosition += dieRoll();
    $bonusWin = 0;

    if ($playerPosition > 19) {
        $playerPosition -= 19;
    }

    $symbolField = substr($fields[+$playerPosition], 0, 1);

    echo $symbolField;
    echo PHP_EOL;

    switch ($symbolField) {
        case 'x':
            $points = substr($fields[+$playerPosition], 1);
            $bonusWin = $points;
            $totalSum += $points;
            break;
        case 'w':
            break;
        case 'h':
            break;
        case 'u':
            $playerPosition = 4; //при масштабируемости игры так, конечно,
            //делать не стоит(как вариант константа в конфиге, или
            //функция, которая будет искать в массиве field нужное поле)
            $bonusWin = $fields[4];
            $totalSum += $fields[4];
            break;
        case 'b':
            $playerPosition = 0;
            $bonusWin = $fields[0];
            $totalSum += $fields[0];
            break;
        case 'p':
            $playerPosition = 6;
            break;
        default:
            return 'error'; //TODO обработчик ошибок
            break;
    }
    echo "позиция игрока " . $playerPosition;
    echo PHP_EOL;
    echo "очки текущего хода: " . $bonusWin;
    echo PHP_EOL;
    echo 'сумма очков: ' . $totalSum;
    echo PHP_EOL;
    echo PHP_EOL;
}

function gameLoop()
{
    reductionNumberStrokes();
    changePlayerPositions();
    increaseNumberOfMoves();
}

/*************************** Main code ****************************************/

for ($count = 0; $count < 7; $count++) {
    gameLoop();

    $arrNumberOfMoves[] = $numberOfMoves;
    $arrRemainingNumberOfMoves[] = $remainingNumberOfMoves;
    $arrPlayerPosition[] = $playerPosition;
    $arrBonusWin[] = $bonusWin;
    $arrTotalSum[] = $totalSum;
}

$arrAnswer = [
    'numberOfMoves' => $arrNumberOfMoves,
    'remainingNumberOfMoves' => $arrRemainingNumberOfMoves,
    'playerPosition' => $arrPlayerPosition,
    'bonusWin' => $arrBonusWin,
    'totalSum' => $arrTotalSum,
];

$jsonAnswer = json_encode($arrAnswer);
echo $jsonAnswer;


//Предупреждение всякому входящему: серверная часть программистом раньше не
//писалась и так как времени в обрез и изучть эту обширную тему можно долго, то
//используется простейший сервер, взятый из замечательной статьи с хабра
//URL: https://habrahabr.ru/post/209864/
//UPD в итоге всё написано на

//$socket = stream_socket_server("http://sergisson.byethost7.com/game/server/gameLogic.php", $errno, $errstr); //tcp://0.0.0.0:8000
//
//if (!$socket) {
//    die("$errstr ($errno)\n");
//}
//
//$connects = array();
//while (true) {
//    //формируем массив прослушиваемых сокетов:
//    $read = $connects;
//    $read []= $socket;
//    $write = $except = null;
//
//    if (!stream_select($read, $write, $except, null)) { //ожидаем сокеты доступные для чтения (без таймаута)
//        break;
//    }
//
//    if (in_array($socket, $read)) { //есть новое соединение
//        $connect = stream_socket_accept($socket, -1); //принимаем новое соединение
//        $connects[] = $connect; //добавляем его в список необходимых для обработки
//        unset($read[ array_search($socket, $read) ]);
//    }
//
//    foreach ($read as $connect) { //обрабатываем все соединения
//        $headers = '';
//        while ($buffer = rtrim(fgets($connect))) {
//            $headers .= $buffer;
//        }
//
//        gameLoop();
//
//        $arrHttpBody = [
//            'numberOfMoves' => $numberOfMoves,
//            'remainingNumberOfMoves' => $remainingNumberOfMoves,
//            'playerPosition' => $playerPosition,
//            'bonusWin' => $bonusWin,
//            'totalSum' => $totalSum,
//        ];
//        $httpBody = json_encode($arrHttpBody);
//
//        fwrite($connect, $httpHeaders . "$httpBody");
//        fclose($connect);
//        unset($connects[ array_search($connect, $connects) ]);
//    }
//}
//
//fclose($server);
