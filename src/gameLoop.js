
/**************************************Functions***********************************************************************/

//TODO обработчик событий когда координата предыдущего хода меньше, чем текущего (происходит из-за переходов
//TODO нужен перенос на соотвествующие поля)

//TODO разобраться как сделать переход pimp-ы между клетками
//TODO анимация руки, пиксели посчитаны, осталось запустить. Не получается весь вечер :( Надо идти отдыхать....

function drowAll(pimpaX, pimpaY, countHands, isAnim) {
    //game.clearRect(0, 0, game.width, game.height); //очищаем холст

    imgBg.onload = function() {      // Событие onLoad, ждём момента пока загрузится изображение

        ctx.drawImage(imgBg, 0, 0);
        ctx.drawImage(imgPimpa, pimpaX, pimpaY, 42, 44.4); //хм, странно, но на видео фишка явно больше, чем исходное
        // изображение (увеличил на всякий случай в 1,2 раза)
        for (count = 0, x = 360; count < countHands - 1; count++, x += 40) {
            ctx.drawImage(imgHand, 0, 0, 50, 100, x, 200, 50, 100); //y = 200
        }
        ctx.drawImage(imgHand, 0, 0, 50, 100, 600, 200, 50, 100); //y = 200; x = 600

        if (isAnim) {
            var handX = 0;
            setTimeout(animHand, 100, handX);
        }

        function animHand(x) {
            ctx.clearRect(0, 0, game.width, game.height); //очищаем холст

            ctx.drawImage(imgBg, 0, 0);
            ctx.drawImage(imgPimpa, pimpaX, pimpaY, 42, 44.4); //хм, странно, но на видео фишка явно больше, чем исходное
            // изображение (увеличил на всякий случай в 1,2 раза)
            for (count = 0, hx = 360; count < countHands - 1; count++, hx += 40) {
                ctx.drawImage(imgHand, 0, 0, 50, 100, hx, 200, 50, 100); //y = 200
            }
            ctx.drawImage(imgDice, 0, 0);
            ctx.drawImage(imgHand, x, 0, 75, 207, 610, 200, 75, 207);
            handX += 77;

            setTimeout(animHand, 100, handX);
        }
        // ctx.drawImage(imgDice, 0, 0); //TODO Не получается нарисовать после анимации в drawDice()
    }
}

// function drawDice(dice) {
//     imgDice.src = 'img/' + dice + '.png';
//     ctx.drawImage(imgDice, 0, 0);
// }

function oneStepPimpa(playerPosition) {  //протестировать TODO
    if (playerPosition  >= 21) {
        playerPosition -=21;
    }

    if ( playerPosition < horizontalCells - 1 ) {
        pimpaX += 88;
        drowAll(pimpaX, pimpaY, remainingNumberOfMoves, false);
        alert('t1');
    } else if ( (playerPosition >= horizontalCells - 1) && (playerPosition < verticalCells + horizontalCells - 2) ) {
        pimpaY += 78;
        drowAll(pimpaX, pimpaY, remainingNumberOfMoves, false);
        alert('t2');
    } else if ( (playerPosition >= horizontalCells + verticalCells - 2) && (playerPosition <= horizontalCells * 2 + verticalCells - 4) ) {
        pimpaX -= 88;
        drowAll(pimpaX, pimpaY, remainingNumberOfMoves, false);
        alert('t3');
    } else if ( (playerPosition >= horizontalCells * 2 - 2 + verticalCells - 1) && (playerPosition <= horizontalCells * 2 + verticalCells * 2 - 5) ) {
        pimpaY -= 78;
        drowAll(pimpaX, pimpaY, remainingNumberOfMoves, false);
        alert('t4');
    }

    // alert(playerPosition + ' ' + horizontalCells);
}

function stepPimpa(currentPosition, dice) {
    if (dice > 6 ) { // || currentPosition > 22
        return 'error!'; //TODO обработчик ошибок
    }

    position = currentPosition;

    for (count = 0; count < dice; count++) {
        oneStepPimpa(position);
        position++;
    }
}

function setEmptyDice(curNumberOfMoves) {
    document.getElementById('empty-dice').innerHTML = curNumberOfMoves + ' EMPTY DICE\'S';
}

function setBonusWin(points) {
    document.getElementById('bonus-win').innerHTML = points;
}

function setTotalBonus(points) {
    document.getElementById('total-bonus-win').innerHTML = points;
}

/**************************************Initialization******************************************************************/

var game = document.getElementById("game"),
    ctx  = game.getContext('2d'),
    imgBg  = new Image(),
    imgPimpa = new Image(),
    imgHand = new Image(),
    imgDice = new Image(),
    pimpaX = 210, pimpaY = 140,
    horizontalCells = 7, verticalCells = 5,
    remainingNumberOfMoves = 7, bonusWin = 0, totalBonusWin = 0, playerPosition = 0;

imgBg.src = 'img/Bg.jpg';
imgPimpa.src = 'img/Pimpa.png';
imgHand.src = 'img/handsAnim.png';

drowAll(pimpaX, pimpaY, remainingNumberOfMoves, false);

setBonusWin(bonusWin); //С другой стороны можно просто проставить значения в div-ах. Надо ли так, не знаю.
setEmptyDice(remainingNumberOfMoves);
setTotalBonus(totalBonusWin);

/**************************************Main code***********************************************************************/

// drowAll(pimpaX, pimpaY, remainingNumberOfMoves, true);



//drowAll(310, 140, 7);

// playerPosition = 1;
// pimpaX += 88 * 1;
// pimpaY += 78 * 4;
// // // setTimeout(oneStepPimpa, 1000, playerPosition);
// //
// stepPimpa(playerPosition, 3);

// drowAll(pimpaX, pimpaY, remainingNumberOfMoves);


// ctx.drawImage(imgHand, 0, 0);
// ctx.clearRect(0,0, 50, 50);
// context.clearRect(0, 0, canvas.width, canvas.height);
// stepPimpa(1, 6);
// oneStepPimpa(1);



/**************************************Click start button**************************************************************/

var isAnswer = false;
var NumberOfMoves = 1;

function turn(){
    if (NumberOfMoves < 8 ) {

        if (!isAnswer) {
            isAnswer = true;
            var xhr = new XMLHttpRequest(),
                url = 'server/gameLogic.php?click=true'; //'http://localhost/test/test.php'; tcp://127.0.0.1:8000

            xhr.open('GET', url, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState != 4) return;

                if (xhr.status != 200) {
                    // обработать ошибку
                    alert('Ошибка ' + xhr.status + ': ' + xhr.statusText);
                    return;
                }
                alert(xhr.responseText);

                answer = JSON.parse(xhr.responseText);
                setBonusWin(answer.bonusWin[0]);
                setTotalBonus(answer.totalSum[0])
                setEmptyDice(answer.remainingNumberOfMoves[0]);
                stepPimpa(0, answer.playerPosition[0] - playerPosition);

                playerPosition = answer.playerPosition[NumberOfMoves];

                // alert('NumberOfMoves: ' + NumberOfMoves + ' осталось: ' + answer.remainingNumberOfMoves[NumberOfMoves]);
            }
            xhr.send(null);
        } else {
            setBonusWin(answer.bonusWin[NumberOfMoves - 1]);
            setTotalBonus(answer.totalSum[NumberOfMoves - 1])

            alert('NumberOfMoves: ' + NumberOfMoves + ' осталось: ' + answer.remainingNumberOfMoves[NumberOfMoves - 1]);
            setEmptyDice(answer.remainingNumberOfMoves[NumberOfMoves - 1]);

            stepPimpa(playerPosition, answer.playerPosition[NumberOfMoves - 1] - playerPosition);
            playerPosition = answer.playerPosition[NumberOfMoves - 1];
        }

        NumberOfMoves++;

        if (NumberOfMoves >= 8) {
            alert('Поздравляем! \n\r Вы набрали: ' + answer.totalSum[6] + ' очков');
        }
    } else {  //game results
        alert('Мы снова вас поздравляем! \n\r Вы набрали: ' + answer.totalSum[6] + ' очков \n\r Но игра уже окончена ;)');
    }
    stepPimpa(0, answer.playerPosition - playerPosition);
}